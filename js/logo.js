/**
 * It's now up to the brave souls to optimize this code.
 */
$(function() {
    var canvas = document.getElementById('presentation-canvas');
    var width = $("#logo-container").width();
    var height = $("#logo-container").height();

    var view = new TorqueData.CanvasView(canvas, width, height);
    canvas.width = width;
    canvas.height = height;
    canvas.style.width = width + "px";
    canvas.style.height = height + "px";
    var colors = ["#FFFFFF", "#f36923"];
    var letterIds = ["#g3047", "#g3015", "#g3019", "#g3023", "#g3027",
        "#g3031", "#g3043", "#g3035", "#g3051", "#g3039"
    ];
    //initialize random points in canvas
    var points = d3.range(400).map(function() {
        var point = new TorqueData.Point();
        point.id = generateUUID();
        point.x = view.getWidth() * Math.random();
        point.y = view.getHeight() * Math.random();
        point.xLoc = 0;
        point.yLoc = 0;
        point.size = 1;
        point.fill = pickRandom(colors);
        point.stroke = point.fill;
        return point;
    });
    for (i = 0; i < points.length; i++) {
        points[i].draw(view);
    }

    var txtfrom = new TorqueData.Point();
    txtfrom.x = view.getWidth() - 5;
    var txtTo = new TorqueData.Point();
    txtTo.x = 50;

    var txtTo2 = new TorqueData.Point();
    txtTo2.x = 0;

    //init text transition points
    var step1Duration = 3000;
    var txt1 = new TorqueData.Text();
    txt1.id = "txt1";
    txt1.text = "We use real-time and historical data";
    txt1.containerId = "logo-container";
    txt1.style = "height: 60px; position: absolute; left: 50px; top: 300px;" +
        "color:#FFFFFF;font-size: 60px;font-family: MrsEaves, myEaves;" +
        "transform: translateX(400px);";
    txt1.fadeInDuration = 2000;
    txt1.appendToContainer(true);

    var txt2 = new TorqueData.Text();
    txt2.id = "txt2";
    txt2.text = "to help you make smarter business decisions.";
    txt2.containerId = "logo-container";
    txt2.style = "height: 60px; position: absolute; left: 50px; top: 360px;" +
        "color:#FFFFFF;font-size: 42px;font-family: Apex New, myFont;" +
        "transform: translateX(400px);";
    txt2.fadeInDuration = txt1.fadeInDuration;
    txt2.appendToContainer(true);

    var step2Duration = 5000;
    var txt3 = new TorqueData.Text();
    txt3.id = "txt3";
    txt3.text = "We use predictive analytics";
    txt3.containerId = "logo-container";
    txt3.style = "height: 60px; position: absolute; left: 50px; top: 300px;" +
        "color:#FFFFFF;font-size: 60px;font-family: MrsEaves, myEaves;" +
        "transform: translateX(400px);";
    txt3.fadeOutDuration = 2000;
    txt3.appendToContainer(true);

    var txt4 = new TorqueData.Text();
    txt4.id = "txt4";
    txt4.text = "to unlock the power of your data…";
    txt4.containerId = "logo-container";
    txt4.style = "height: 60px; position: absolute; left: 50px; top: 360px;" +
        "color:#FFFFFF;font-size: 42px;font-family: Apex New, myFont;" +
        "transform: translateX(400px);"; 
    txt4.fadeOutDuration = 2000;  
    txt4.appendToContainer(true);

    var step3Duration = 5000;
    var txt5 = new TorqueData.Text();    
    txt5.id = "txt5";
    txt5.text = "…to forecast your customers’";
    txt5.containerId = "logo-container";
    txt5.style = "height: 60px; position: absolute; left: 50px; top: 300px;" +
        "color:#FFFFFF;font-size: 60px;font-family: MrsEaves, myEaves;" +
        "transform: translateX(400px);";
    txt5.appendToContainer(true);

    var txt6 = new TorqueData.Text();
    txt6.id = "txt6";
    txt6.text = "behavior and maximize their value…";
    txt6.containerId = "logo-container";
    txt6.style = "height: 60px; position: absolute; left: 50px; top: 360px;" +
        "color:#FFFFFF;font-size: 42px;font-family: Apex New, myFont;" +
        "transform: translateX(400px);";
    txt6.appendToContainer(true);

    var step4Duration = 5000;
    var txt7 = new TorqueData.Text();
    txt7.id = "txt7";
    txt7.text = "…Increase the efficiency";
    txt7.containerId = "logo-container";
    txt7.style = "height: 60px; position: absolute; left: 50px; top: 300px;" +
        "color:#FFFFFF;font-size: 60px;font-family: MrsEaves, myEaves;" +
        "transform: translateX(400px);";
    txt7.fadeOutDuration = 5000;
    txt7.appendToContainer(true);

    var txt8 = new TorqueData.Text();
    txt8.id = "txt8";
    txt8.text = "of your sales channels…";
    txt8.containerId = "logo-container";
    txt8.style = "height: 60px; position: absolute; left: 50px; top: 360px;" +
        "color:#FFFFFF;font-size: 42px;font-family: Apex New, myFont;" +
        "transform: translateX(400px);";
    txt8.fadeOutDuration = 5000;
    txt8.appendToContainer(true);

    var step5Duration = 5000;
    var txt9 = new TorqueData.Text();
    txt9.id = "txt9";
    txt9.text = "…Expand the impact";
    txt9.containerId = "logo-container";
    txt9.style = "height: 60px; position: absolute; left: 50px; top: 300px;" +
        "color:#FFFFFF;font-size: 60px;font-family: MrsEaves, myEaves;" +
        "transform: translateX(400px);";
    txt9.appendToContainer(true);

    var txt10 = new TorqueData.Text();
    txt10.id = "txt10";
    txt10.text = "of your media...";
    txt10.containerId = "logo-container";
    txt10.style = "height: 60px; position: absolute; left: 50px; top: 360px;" +
        "color:#FFFFFF;font-size: 42px;font-family: Apex New, myFont;" +
        "transform: translateX(400px);";
    txt10.appendToContainer(true);

    var step6Duration = 5000;
    var txt11 = new TorqueData.Text();
    txt11.id = "txt11";
    txt11.text = "…And place you";
    txt11.containerId = "logo-container";
    txt11.style = "height: 60px; position: absolute; left: 50px; top: 300px;" +
        "color:#FFFFFF;font-size: 60px;font-family: MrsEaves, myEaves;" +
        "transform: translateX(400px);";
    txt11.appendToContainer(true);

    var txt12 = new TorqueData.Text();
    txt12.id = "txt12";
    txt12.text = "one step ahead of the pack.";
    txt12.containerId = "logo-container";
    txt12.style = "height: 60px; position: absolute; left: 50px; top: 360px;" +
        "color:#FFFFFF;font-size: 42px;font-family: Apex New, myFont;" +
        "transform: translateX(400px);";
    txt12.appendToContainer(true);


    fadeOutO(letterIds, 0);
    //setup chain of events and then start
    var chain = new TorqueData.TimeoutChain();
    var xy = null;
    chain.enqueue(function() {
        dancing(view, points, "logo-container", "g3015", chain, true);
    }, 0, 3000);
    chain.enqueue(function() {
        dancing(view, points, "logo-container", "g3015", chain, false);
    }, 0, 2000);
    chain.enqueue(function() {
        swarm(view, points, 3000, {
            x: 0,
            y: 0
        }, 0);
        fadeInO(letterIds, 2000);
    }, 0, 2000);
    chain.enqueue(function() {
        fadeOutLogo(letterIds, 1000);
    }, 0, 1000);
    chain.enqueue(function() {
        absorb(view, points, step1Duration, 0, 0.35);
        txt1.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt1.fadeInDuration);
        txt2.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt1.fadeInDuration);
    }, 0, step1Duration);
    chain.enqueue(function() {
        swarm(view, points, 3000, {
            x: 0,
            y: 0
        }, 0);
    }, 0, 3000);
    chain.enqueue(function() {
        goBelow(view, points, txt1.fadeOutDuration + step2Duration);
        txt1.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt1.fadeOutDuration);
        txt2.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt2.fadeOutDuration);
        lineBelow(view, txt1.fadeOutDuration + step2Duration);
    }, 0, txt1.fadeOutDuration);
    chain.enqueue(function() {
        txt3.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt3.fadeInDuration);
        txt4.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt4.fadeInDuration);
    }, 0, step2Duration);
    chain.enqueue(function() {
        xy = lineChartLayout(view, points, step3Duration);
        txt3.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt3.fadeOutDuration);
        txt4.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt4.fadeOutDuration);
        TWEEN.removeAll();
    }, 0, step3Duration);
    chain.enqueue(function() {
        var lineDuration = step3Duration;        
        txt5.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt5.fadeInDuration);
        txt6.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt6.fadeInDuration);
        lineInChart(view, points, xy, lineDuration, 0);
    }, 0, step3Duration);
    chain.enqueue(function() {
        toTheSides(view, points, txt5.fadeOutDuration + step4Duration);
        txt5.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt5.fadeOutDuration);
        txt6.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt6.fadeOutDuration);
        TWEEN.removeAll();
    }, 0, txt5.fadeOutDuration);
    chain.enqueue(function() {
        txt7.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt7.fadeInDuration);
        txt8.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt8.fadeInDuration);
    }, 0, step4Duration);
    chain.enqueue(function() {
        absorb(view, points, txt7.fadeOutDuration, 0, 0.45);
        txt7.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt7.fadeOutDuration);
        txt8.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt7.fadeOutDuration);
        TWEEN.removeAll();
    }, 0, txt7.fadeOutDuration);
    chain.enqueue(function() {
       circular(view, points, 2000);
    }, 0, 2000);
    chain.enqueue(function() {
        randomMotion(view, points, step5Duration + txt9.fadeOutDuration);
        txt9.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt9.fadeInDuration);
        txt10.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt10.fadeInDuration);
    }, 0, step5Duration);
    chain.enqueue(function() {
        txt9.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt9.fadeOutDuration);
        txt10.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt10.fadeOutDuration);
        TWEEN.removeAll();
    }, 0, txt9.fadeOutDuration);
    chain.enqueue(function() {
        txt11.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt11.fadeInDuration);
        txt12.slideIn(txtfrom, txtTo, TWEEN.Easing.Linear.None, txt12.fadeInDuration);
        explosion(view, points, step6Duration);
    }, 0, step6Duration);
    chain.enqueue(function() {
        txt11.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt11.fadeOutDuration);
        txt12.slideOut(txtTo, txtTo2, TWEEN.Easing.Linear.None, txt12.fadeOutDuration);
        TWEEN.removeAll();
    }, 0, txt11.fadeOutDuration);
    chain.enqueue(function() {
        show(letterIds, 2000);
    }, 0, 3000);

    chain.start();

    window.addEventListener('resize', resize, false);

    function resize() {
        var width = $("#logo-container").width();
        var height = $("#logo-container").height();
        canvas.width = width;
        canvas.height = height;
        canvas.style.width = width + "px";
        canvas.style.height = height + "px";
        txtfrom.x = view.getWidth() - 5;
        txtTo.x = 50;
    }

});

var fadeInO = function(letterIds, duration) {
    var copyOfLetterIds = letterIds.slice();
    var o = copyOfLetterIds.splice(1, 1);
    $(o[0]).css({
        "visibility": "visible"
    }).hide(0).fadeIn(duration);
}

var fadeOutO = function(letterIds, duration) {
    var copyOfLetterIds = letterIds.slice();
    var o = copyOfLetterIds.splice(1, 1);
    $(o[0]).css({
        "visibility": "hidden"
    }).fadeIn(duration);
}

var fadeOutLogo = function(letterIds, duration) {
    fadeOut(letterIds, duration);
}

var pickRandom = function(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

var show = function(arr, duration) {
    for (i = 0; i < arr.length; i++) {
        $(arr[i]).show(duration);
    }
}

var hide = function(arr, duration) {
    for (i = 0; i < arr.length; i++) {
        $(arr[i]).hide(duration);
    }
}

var fadeOut = function(arr, duration) {
    for (i = 0; i < arr.length; i++) {
        $(arr[i]).fadeOut(duration);
    }
}

var fadeIn = function(arr, duration) {
    for (i = 0; i < arr.length; i++) {
        $(arr[i]).fadeIn(duration);
    }
}

var swarm = function(view, points, duration, pointOfOrigin, sliced) {
    var data = points.slice(0, points.length - sliced), stopped = false;
    var stopper = setInterval(function() {
        stopped = true;
    }, duration);
    var x = d3.scale.linear()
        .domain([-5, 5])
        .range([0, view.getWidth()]);
    var y = d3.scale.linear()
        .domain([-5, 5])
        .range([0, view.getHeight()]);

    for (i = 0; i < data.length; i++) {
        d = data[i];
        d.xLoc = pointOfOrigin.x;
        d.yLoc = pointOfOrigin.y;
    }

    d3.timer(function() {

        view.clearAll();

        for (i = 0; i < data.length; i++) {
            d = data[i];
            d.xLoc += d.xVel;
            d.yLoc += d.yVel;
            d.xVel += 0.04 * (Math.random() - .5) - 0.05 * d.xVel - 0.0005 * d.xLoc;
            d.yVel += 0.04 * (Math.random() - .5) - 0.05 * d.yVel - 0.0005 * d.yLoc;

            d.size = 3;
            d.x = x(d.xLoc);
            d.y = y(d.yLoc);
            d.draw(view);
        }

        if (stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });

}

var lineChartLayout = function(view, points, duration) {
    var data = points,
        item, dest;
    var stopped = false;
    var temp = [];
    var yStep = 17;
    var xStep = 25;
    var xNext = 0;
    var yNext = 0;
    var y = 0;
    var x = 0;
    var friction = 0.08;

    for (y = 0; y < yStep; y++) {
        yNext += Math.ceil(view.getHeight() / yStep);
        xNext = 0;
        for (x = 0; x < xStep; x++) {
            dest = new TorqueData.Point();
            xNext += Math.ceil(view.getWidth() / xStep);
            dest.x = xNext;
            dest.y = yNext;
            temp[temp.length] = dest;
        }
    }

    var stopper = setInterval(function() {
        stopped = true;
    }, duration);
    
    d3.timer(function() {
        view.clearAll();
        for (i = 0; i < Math.min(temp.length, data.length); i++) {
            item = data[i];
            dest = temp[i];
            item.size = 3;
            item.xVel += (dest.x - item.x) * (Math.random() * 0.09);
            item.yVel += (dest.y - item.y) * (Math.random() * 0.09);
            item.x += item.xVel;
            item.y += item.yVel;
            item.xVel *= friction;
            item.yVel *= friction;
            item.draw(view);
        }

        if (stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });
    return {
        x: x,
        y: y,
        xStep: xStep,
        yStep: yStep
    };
}

var lineInChart = function(view, data, xy, duration, delay) {
    var tweenDuration = duration * 0.045;
    var tweenDelay = delay;
    var yStep = 17;
    var xStep = 25;
    var xNext = 0;
    var yNext = 0;
    var y = 0;
    var x = 0;

    for (y = 0; y < yStep; y++) {
        yNext += Math.ceil(view.getHeight() / yStep);
        xNext = 0;
        for (x = 0; x < xStep; x++) {
            xNext += Math.ceil(view.getWidth() / xStep);
        }
    }

    var line1 = new TorqueData.Line();
    line1.from = data[(y - 2) * xStep];
    line1.to = data[(y - 5) * xStep + 3];
    line1.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line2 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line2.from = data[(y - 5) * xStep + 3];
    line2.to = data[(y - 4) * xStep + 4];
    line2.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line3 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line3.from = data[(y - 4) * xStep + 4];
    line3.to = data[(y - 13) * xStep + 7];
    line3.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line4 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line4.from = data[(y - 13) * xStep + 7];
    line4.to = data[(y - 12) * xStep + 8];
    line4.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line5 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line5.from = data[(y - 12) * xStep + 8];
    line5.to = data[(y - 14) * xStep + 11];
    line5.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line6 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line6.from = data[(y - 14) * xStep + 11];
    line6.to = data[(y - 13) * xStep + 14];
    line6.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line7 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line7.from = data[(y - 13) * xStep + 14];
    line7.to = data[(y - 16) * xStep + 19];
    line7.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line8 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line8.from = data[(y - 16) * xStep + 19];
    line8.to = data[(y - 15) * xStep + 20];
    line8.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);

    var line9 = new TorqueData.Line();
    tweenDelay += tweenDuration;
    line9.from = data[(y - 15) * xStep + 20];
    line9.to = data[(y - 17) * xStep + 23];
    line9.tween(view, tweenDuration, tweenDelay, TWEEN.Easing.Linear.None, true);
}

var absorb = function(view, points, duration, sliced, friction) {
    var stopped = false, item;
    var data = points.slice(0, points.length - sliced);

    var stopper = setInterval(function() {
        stopped = true;
    }, duration);
    var center = {x: view.getWidth() / 2, y: view.getHeight() / 2};

    d3.timer(function() {
        center = {x: view.getWidth() / 2, y: view.getHeight() / 2};
        view.clearAll();
        for (i = 0; i < data.length; i++) {
            item = data[i];
            item.xVel += ((center.x - item.x) * Math.random() * 0.05);
            item.yVel += ((center.y - item.y) * Math.random() * 0.05);
            item.xVel *= friction;
            item.yVel *= friction;
            item.x += item.xVel;
            item.y += item.yVel;
            item.draw(view);
        }

        if (stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });
}

var generateUUID = function() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
        .replace(/[xy]/g, function(c) {

            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
        });
    return uuid;
}

var dancing = function(view, points, parentContainerId, containerId, chain, isOCentered) {
    var p = document.getElementById(parentContainerId);
    var o = document.getElementById(containerId);
    var center;
    var data = points.slice(0, points.length - 200);
    _ref = data;
    for (count = 0; count < _ref.length; ++count) {
        item = _ref[count];
        item.index = count;
        item.friend = chooseFriend(item, data);
    }

    var center, coef, cx, cy, d, floor, friend, indice, item, offset, opposite,
        others, px, py, size, _i, _j, _len, _len1;

    offset = 0;
    coef = 0.7;
    friend = 0.06 / 2 * coef * 1.4;
    floor = 0.05 / 7 * coef * 9;
    others = 0.1 / 5000 * coef;

    var stopped = false;
    var stopper = setInterval(function() {
        stopped = true;
    }, chain.actualDuration(0));

    d3.timer(function() {
        if(isOCentered)
            center = getPosition(o);
        else
            center = { x: view.getWidth() / 2, y: view.getHeight() / 2 };

        view.clearAll();

        for (_i = 0, _len = data.length; _i < _len; _i++) {
            item = data[_i];
            cx = item.x - center.x;
            cy = item.y - center.y;
            item.x -= cx * floor;
            item.y -= cy * floor;
            for (_j = 0, _len1 = data.length; _j < _len1; _j++) {
                opposite = data[_j];
                if (item.id !== opposite.id) {
                    indice = 4.5;
                    px = item.x - opposite.x;
                    py = item.y - opposite.y;
                    d = Math.sqrt(px * px + py * py);
                    if (d < 10) {
                        indice = 3000;
                    } else if (d > 300) {
                        indice = 0.5;
                    }
                    item.x -= (opposite.x - item.x) * (others * indice);
                    item.y -= (opposite.y - item.y) * (others * indice);
                }
            }
            item.x += (item.friend.x - item.x) * friend;
            item.y += (item.friend.y - item.y) * friend;
            if (Math.floor(Math.random() * 15000) === 13) {
                item.friend = chooseFriend(item, data);
            }
            item.draw(view);
        }
        if (stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });
}

var chooseFriend = function(obj, array) {
    var friend, friendIndex, reassign;
    friendIndex = Math.floor(Math.random() * (array.length - 1));
    friend = array[friendIndex];
    reassign = function(obj, opposite, index) {
        if (opposite.id === obj.id) {
            if (index + 1 < array.length - 1) {
                return opposite = friendIndex + 1;
            } else if (index - 1 > 0) {
                return opposite = friendIndex - 1;
            } else {
                index = Math.floor(Math.random() * (array.length - 1));
                opposite = array[index];
                return reassign(obj, opposite, index);
            }
        }
    };
    if (array.length > 2) {
        reassign(obj, friend, friendIndex);
    }
    return friend;
}

var lineBelow = function(view, duration) {
    var linePosition = (view.getHeight() - 1) * 0.75;
    var centerX = view.getWidth() / 2, x1, x2;
    var stopped = false;
    var point1 = new TorqueData.Point();
    var point2 = new TorqueData.Point();
    var point3 = new TorqueData.Point();
    var point4 = new TorqueData.Point();
    point1.x = 0;
    point1.y = linePosition;
    point2.x = centerX;
    point2.y = linePosition;
    point3.x = view.getWidth() - 1;
    point3.y = linePosition;
    point4.x = centerX;
    point4.y = linePosition;

    var lineFromLeft = new TorqueData.Line(point1, point2, 2);
    lineFromLeft.stroke = "#f36923";
    lineFromLeft.tween(view, duration, 0, TWEEN.Easing.Linear.None, false);
    var lineFromRight = new TorqueData.Line(point3, point4, 2);
    lineFromRight.stroke = "#f36923";
    lineFromRight.tween(view, duration, 0, TWEEN.Easing.Linear.None, false);
}


var goBelow = function(view, points, duration) {
    var stopped = false, friction = 0.45;
    var temp = [], item, dest, i;
    var linePosition = (view.getHeight() - 1) * 0.75;
    var centerX = Math.ceil(view.getWidth() / 2);

    for (i = 0; i < points.length; i++) {
        dest = new TorqueData.Point();
        dest.xLoc = points[i].x;
        dest.x = centerX;
        dest.y = linePosition + 50 + ((view.getHeight() - linePosition) * Math.random()); //random position below the horizontal line
        temp[i] = dest;
        points[i].xVel = 0;
        points[i].yVel = 0;
        points[i].yLoc = 0;
    }

    var stopper = setInterval(function() {
        stopped = true;
    }, duration);

    d3.timer(function() {
        view.clearAll();

        for (i = 0; i < points.length; i++) {
            item = points[i];
            dest = temp[i];

            if(item.y >= linePosition) {
                dest.x = dest.xLoc;
            }

            item.xVel += ((dest.x - item.x) * Math.random() * 0.05);
            item.yVel += ((dest.y - item.y) * Math.random() * 0.05);
            item.xVel *= friction;
            item.yVel *= friction;
            item.x += item.xVel;
            item.y += item.yVel;
            item.draw(view);
        }

        if (stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });
}

var randomMotion = function(view, points, duration) {
    var stopped = false, i, j, stopper, repaint = false, item, friction = 0.98;
    var closest = [];
    var current, distance;
    for (i = 0; i < points.length; i++) {
        points[i].xVel = 0;
        points[i].yVel = 0;
    }
    i = 0;
    /*for (i = 0; i < 5; i++) {*/
    current = points[i];
    for (j = 0; j < points.length; j++) {
        if(current.id != points[j].id) {
            distance = current.distance(points[j]);
            if(!closest[i]) 
                closest[i] = { index: j, dist: distance}; 
            else {
                if(distance < closest[i].dist) {
                    closest[i] = {index: j, dist: distance};
                }
            }
        }
    }
    /*}*/
    var line1 = new TorqueData.Line(current, points[closest[i].index], 2);
    stopper = setInterval(function () {
        stopped = true;
    }, duration);

    d3.timer(function () {
        view.clearAll();
        for(i = 0; i < points.length; i++) {
            item = points[i];
            brownian(item, friction, .1, .2);            
            item.draw(view);
        }
        line1.draw(view);
        
        if(stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });
}

var getPosition = function(element) {
    var elemRect = element.getBoundingClientRect();
    var yPosition = (elemRect.top + elemRect.bottom) / 2;
    var xPosition = (elemRect.left + elemRect.right) / 2;
    return {
        x: xPosition,
        y: yPosition
    };
}

var getPosition2 = function(parent, element) {
    var parentRect = element.getBoundingClientRect();
    var elemRect = element.getBoundingClientRect();
    var yPosition = elemRect.top + ((elemRect.bottom - elemRect.top) / 2) -
            parentRect.top;
    var xPosition = elemRect.left + ((elemRect.right - elemRect.left) / 2) -
            parentRect.left;
    return {
        x: xPosition,
        y: yPosition
    }; 
}

var brownian = function (item, friction, from, to) {
    item.xVel += Math.random() * to - from;
    item.yVel += Math.random() * to - from;
    item.x += item.xVel;
    item.y += item.yVel;
    item.xVel *= friction;
    item.yVel *= friction;
}

var toTheSides = function(view, points, duration) {
    var stopped = false, friction = 0.45;
    var center = {
        x: view.getWidth() / 2,
        y: view.getHeight() / 2
    };
    var item, dest = new TorqueData.Point();
    var stopper = setInterval(function() {
        stopped = true;
    }, duration);

    for (var i = 0; i < points.length; i++) {
        item = points[i];
        item.xVel = 0;
        item.yVel = 0;
        dest.x = 0;
        dest.y = 0;
    }

    d3.timer(function() {
        view.clearAll();
        for (var i = 0; i < points.length; i++) {
            item = points[i];
            if (item.x <= center.x && item.y <= center.y) {
                dest.x = 0;
                dest.y = 0;
            } else if (item.x > center.x && item.y < center.y) {
                dest.x = view.getWidth() - 1;
                dest.y = 0;
            } else if (item.x > center.x && item.y > center.y) {
                dest.x = view.getWidth() - 1;
                dest.y = view.getHeight() - 1;
            } else if (item.x < center.x && item.y > center.y) {
                dest.x = 0;
                dest.y = view.getHeight() - 1;
            }

            item.xVel += ((dest.x - item.x) * Math.random() * 0.05);
            item.yVel += ((dest.y - item.y) * Math.random() * 0.05);
            item.xVel *= friction;
            item.yVel *= friction;
            item.x += item.xVel;
            item.y += item.yVel;
            item.draw(view);
        }

        if (stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });
}

var circularDistribution = function(item, center, mainRadius, mainAngle) {
    var radius = Math.random() * mainRadius;
    var angle = Math.random() * mainAngle;
    item.xLoc = center.x + (Math.cos(angle) * radius);
    item.yLoc = center.y + (Math.sin(angle) * radius);
}

var circular = function(view, points, duration) {
    var stopped = false, friction = 0.60, i, item;
    var stopper = setInterval(function () {
        stopped = true;
    }, duration);
    var center = { x: view.getWidth() / 2, y: view.getHeight() / 2};

    for(i = 0; i < points.length; i++) {
        circularDistribution(points[i], center, center.x / 2, Math.PI * 2);
    }

    d3.timer(function() {
        view.clearAll();
        for(i = 0; i < points.length; i++) {
            item = points[i];
            item.xVel += ((item.xLoc - item.x) * Math.random() * 0.05);
            item.yVel += ((item.yLoc - item.y) * Math.random() * 0.05);
            item.xVel *= friction;
            item.yVel *= friction;
            item.x += item.xVel;
            item.y += item.yVel;
            item.draw(view);
        }
        if(stopped) {
            clearInterval(stopper);
        }
        return stopped;
    });
}

var explosion = function(view, points, duration) {
    var stopped = false, friction = 1.02, i, item;
    var stopper = setInterval(function () {
        stopped = true;
    }, duration);
    var data = points.slice(0, points.length - 200);
    for(i = 0; i < data.length; i++) {
        item = data[i];
        item.xVel = -5 + Math.random()*10;
        item.yVel = -5 + Math.random()*10;
    }
    d3.timer(function() {
        view.clearAll();
        for(i = 0; i < data.length; i++) {
            item = data[i];
            item.x += item.xVel;
            item.y += item.yVel;
            item.xVel *= friction;
            item.yVel *= friction;
            item.size = Math.min(item.size + 0.5, 20);
            item.draw(view);
        }
        if(stopped) {
            clearInterval(stopper);
            view.clearAll();
        }
        return stopped;
    });
}
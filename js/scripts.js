window.TorqueData = {};

/**
 * Defines a point.
 *
 */
window.TorqueData.Point = function() {
    function Point() {
        this.id = null;
        this.x = 0;
        this.y = 0;
        this.size = 2;
        this.start = 0;
        this.end = Math.PI * 2;
        this.isClockwise = false;
        this.fill = "#f36923"
        this.stroke = "#f36923"
        this.xVel = 0;
        this.xLoc = 0;
        this.yLoc = 0;
        this.yVel = 0;
    }

    Point.prototype.draw = function(view) {
        var ratio = view.getPixelRatio();
        view.context.beginPath();
        view.context.arc(this.x , this.y, this.size,
            this.start, this.end, this.isClockwise);
        view.fill(this.fill);
        view.stroke(this.stroke);
    };

    Point.prototype.description = function() {
        return "This is a point at (x,y): (" + this.x + "," + this.y + ")" +
            "\n moving at " + this.xVel + " on x and " + this.yVel + " on y.";
    }

    Point.prototype.distance = function(p2) {
        var xDist = this.x - p2.x;
        var yDist = this.y - p2.y;
        return Math.sqrt((xDist * xDist) + (yDist * yDist));
    }

    return Point;
}();

window.TorqueData.TimeoutChain = function() {
    function TimeoutChain() {
        this.chain = new Array();
        this.current = 0;
        this.isRunning = false;
    }

    /**
     * Starts the chain based on which task in the chain
     * that this.current is pointing.
     * @return true if able to start, false if already
     * running or has no tasks in the chain.
     */
    TimeoutChain.prototype.start = function() {
        var ret = false;
        if (this.isRunning != true) {
            if (this.chain.length > 0) {
                this.isRunning = true;
                this.run(this.current);
            }
        }

        return ret;
    }

    TimeoutChain.prototype.stop = function(clear) {
        this.isRunning = false;
        if (clear == true) {
            this.clear();
        }
    }

    /**
     * Clears the chain.
     */
    TimeoutChain.prototype.clear = function() {
        for (i = 0; i < this.chain.length; i++) {
            window.clearTimeout(this.chain[i].handler);
        }
    }

    /**
     * Processes the current step until, if step is < the length
     * of the chain.
     */
    TimeoutChain.prototype.runStep = function(step) {
        if (step < this.chain.length) {
            this.current = step;

            var _this = this;
            var handler = window.setTimeout(function() {
                _this.chain[_this.current].task();
                _this.chain[_this.current].timeStarted = new Date();
                console.log("run step: " + _this.current + " time: " + _this.chain[_this.current].timeStarted);
                _this.next();
            }, this.chain[this.current].actualDelay);
            this.chain[this.current].handler = handler;
        } else {
            this.stop(true);
        }
    }

    TimeoutChain.prototype.next = function() {
        var step = this.current + 1;
        this.runStep(step);
    }

    TimeoutChain.prototype.run = function() {
        this.runStep(this.current);
    }

    /**
     * The computation for the delay is previous delay + previous duration +
     * param delay.
     *
     * You can set the param delay to 0, so that it executes right after
     * the previous task.
     */
    TimeoutChain.prototype.enqueue = function(_function, _delay,
        _duration) {
        var delay = 0;
        if (this.chain.length > 0) {
            delay = this.chain[this.chain.length - 1].delay +
                this.chain[this.chain.length - 1].duration + _delay;
        } else {
            delay = _delay;
        }

        this.chain[this.chain.length] = {
            task: _function,
            delay: _delay,
            actualDelay: delay,
            duration: _duration,
            timeStarted: null,
            handler: null
        };
    }

    TimeoutChain.prototype.duration = function(index) {
        return this.chain[index].duration;
    }

    TimeoutChain.prototype.actualDuration = function(index) {
        return this.duration(index) + this.delay(index);
    }

    TimeoutChain.prototype.delay = function(index) {
        return this.chain[index].delay;
    }

    TimeoutChain.prototype.actualDelay = function(index) {
        return this.chain[index].actualDelay;
    }

    return TimeoutChain;
}();

/**
 * This class requires Tween.js
 *
 */
window.TorqueData.Line = function() {
    function Line() {
        this.width = 2;
        this.from = p1;
        this.to = p2;
        this.stroke = "#FFFFFF"
        this.fill = "#FFFFFF"
    }

    function Line(p1, p2, width) {
        this.width = width;
        this.from = p1;
        this.to = p2;
        this.stroke = "#FFFFFF"
        this.fill = "#FFFFFF"
    }

    Line.prototype.tween = function(view, duration, delay, easing, incremental) {
        var _this = this;
        init();
        animate();

        function init() {
            var _view = view;
            var position = {
                x: _this.from.x,
                y: _this.from.y
            };
            var prevPosition = {
                x: _this.from.x,
                y: _this.from.y
            };
            var tween = new TWEEN.Tween(position)
                .to({
                    x: _this.to.x,
                    y: _this.to.y
                }, duration)
                .easing(easing)
                .onUpdate(function() {
                    if (!incremental) {
                        _this.draw(view, _this.from.x, _this.from.y,
                            position.x, position.y);
                    } else {
                        _this.draw(view, prevPosition.x, prevPosition.y,
                            position.x, position.y);
                    }
                    prevPosition.x = position.x;
                    prevPosition.y = position.y;
                }).delay(delay)
                .start();
        }

        function animate(time) {
            requestAnimationFrame(animate);
            TWEEN.update(time);
        }
    }

    Line.prototype.draw = function(view) {
        this.draw(view, this.from.x, this.from.y, this.to.x, this.to.y);
    }

    Line.prototype.draw = function(view, x1, y1, x2, y2) {
        if (!x1) {
            x1 = this.from.x;
        }
        if(!x2) {
            x2 = this.to.x;
        }
        if(!y1) {
            y1 = this.from.y;
        }
        if(!y2) {
            y2 = this.to.y;
        }
        view.context.beginPath();
        view.context.lineWidth = this.width;
        view.context.moveTo(x1, y1);
        view.context.lineTo(x2, y2);
        view.fill(this.fill);
        view.stroke(this.stroke);
    }

    return Line;
}();

window.TorqueData.Text = function() {
    function Text() {
        this.text = null;
        this.id = null;
        this.containerId = null;
        this.style = null;
        this.fadeInDuration = 2000;
        this.fadeOutDuration = 400;
    }

    Text.prototype.appendToContainer = function(hidden) {
        try {
            var element = "<div id='" + this.id + "' style='" + this.style +
                "'>" + this.text + "</div>";
            var parent = $("#" + this.containerId);

            parent.append(element);

            if (hidden)
                $("#" + this.id).hide();
        } catch (err) {
            console.log(err.message);
        }
    }

    Text.prototype.show = function(duration) {
        try {
            if (duration)
                $("#" + this.id).fadeIn(duration);
            else
                $("#" + this.id).fadeIn(this.fadeInDuration);
        } catch (err) {
            console.log(err.message);
        }
    }

    Text.prototype.hide = function(duration) {
        try {
            if (duration)
                $("#" + this.id).fadeOut(duration);
            else
                $("#" + this.id).fadeOut(this.fadeOutDuration);
        } catch (err) {
            console.log(err.message);
        }
    }

    Text.prototype.slideIn = function(from, to, easing, duration) {
        var _this = this;
        init();
        animate();
        this.show(this.fadeInDuration);

        function init() {
            var position = {
                x: from.x,
                y: from.y
            };
            var tween = new TWEEN.Tween(position)
                .to({
                    x: to.x,
                    y: to.y
                }, duration)
                .easing(easing)
                .onUpdate(function() {
                    var output = $("#" + _this.id);
                    var translateX = 'translateX(' + this.x + 'px)';
                    output.css({
                        "-webkit-transform": translateX
                    });
                    output.css({
                        "transform": translateX
                    });
                })
                .start();
        }

        function animate(time) {
            requestAnimationFrame(animate);
            TWEEN.update(time);
        }
    }

    Text.prototype.slideOut = function(from, to, easing, duration) {
        var _this = this;
        this.hide(this.fadeOutDuration);
        init();
        animate();

        function init() {
            var position = {
                x: from.x,
                y: from.y
            };
            var tween = new TWEEN.Tween(position)
                .to({
                    x: to.x,
                    y: to.y
                }, duration)
                .easing(easing)
                .onUpdate(function() {
                    var output = $("#" + _this.id);
                    var translateX = 'translateX(' + this.x + 'px)';
                    output.css({
                        "-webkit-transform": translateX
                    });
                    output.css({
                        "transform": translateX
                    });
                })
                .start();
        }

        function animate(time) {
            requestAnimationFrame(animate);
            TWEEN.update(time);
        }
    }

    return Text;
}();

/**
 * Things that relate to the canvas.
 **/
window.TorqueData.CanvasView = function() {

    CanvasView = function(canvas, width, height) {
        this.canvas = canvas;
        this.context = canvas.getContext("2d");
        this.context.fillStyle = "#FFFFFF";
        this.context.strokeStyle = "#FFFFFF";
        this.context.strokeWidth = 1.5;
        this.drawables = [];
        return this;
    };

    CanvasView.prototype.getPixelRatio = function() {
        var backingStore = this.context.backingStorePixelRatio ||
            this.context.webkitBackingStorePixelRatio ||
            this.context.mozBackingStorePixelRatio ||
            this.context.msBackingStorePixelRatio ||
            this.context.oBackingStorePixelRatio ||
            this.context.backingStorePixelRatio ||
            1;
        var devicePixelRatio = (window.devicePixelRatio || 1);
        return  devicePixelRatio / backingStore;
    };

    CanvasView.prototype.clear = function(x, y, width, height) {
        this.context.clearRect(x, y, width, height);
    };

    CanvasView.prototype.clearAll = function() {
        this.clear(0, 0, this.getWidth(), this.getHeight());
    };

    CanvasView.prototype.fill = function() {
        this.context.fill();
    };

    CanvasView.prototype.fill = function(style) {
        var tempFillStyle = this.context.fillStyle;
        this.context.fillStyle = style;
        this.context.fill();
        this.context.fillStyle = tempFillStyle;
    };

    CanvasView.prototype.stroke = function() {
        this.context.stroke();
    };

    CanvasView.prototype.stroke = function(style) {
        var tempStrokeStyle = this.context.strokeStyle;
        this.context.strokeStyle = style;
        this.context.stroke();
        this.context.strokeStyle = tempStrokeStyle;
    };

    CanvasView.prototype.getWidth = function() {
        return this.canvas.width;
    };

    CanvasView.prototype.getHeight = function() {
        return this.canvas.height;
    };

    CanvasView.prototype.addDrawable = function(obj) {
        this.drawables.push(obj);
    }

    CanvasView.prototype.hasNothingToDraw = function() {
        return this.drawables.length == 0;
    }

    CanvasView.prototype.removeDrawable = function(index) {
        this.drawables = this.drawables.splice(index, 1);
    }

    CanvasView.prototype.repaint = function() {
        var item;
        this.clearAll();
        for(i = 0; i < this.drawables.length; i++) {
            item = this.drawables[i].draw(this);
        }
    }

    CanvasView.prototype.repaint = function(closure) {
        var item;
        this.clearAll();
        for(i = 0; i < this.drawables.length; i++) {
            closure(this.drawables[i]);
            this.drawables[i].draw(this);
        }
    }

    return CanvasView;
}();